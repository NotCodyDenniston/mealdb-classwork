package com.rave.mealapp.model.remote.dtos.category

import kotlinx.serialization.Serializable

/**
 * The response object given when asking for a list of [Category]
 * objects.
 */
@Serializable
data class CategoryResponse(
    val categories: List<CategoryDTO> = emptyList()
)
