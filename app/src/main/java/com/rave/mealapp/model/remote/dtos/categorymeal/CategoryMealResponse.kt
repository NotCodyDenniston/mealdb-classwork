package com.rave.mealapp.model.remote.dtos.categorymeal

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * The response object for the [Meal] objects that
 * fall within a specific [Category].
 */
@Serializable
data class CategoryMealResponse(
    @SerialName("meals")
    val categoryMeals: List<CategoryMealDTO>
)
