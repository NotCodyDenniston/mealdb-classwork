package com.rave.mealapp.model.mapper

import com.rave.mealapp.model.local.Category
import com.rave.mealapp.model.remote.dtos.category.CategoryDTO

class CategoryMapper : Mapper<CategoryDTO, Category> {
    override fun invoke(dto: CategoryDTO): Category = with(dto) {
        Category(
            idCategory = idCategory,
            strCategory = strCategory,
            strCategoryDescription = strCategoryDescription,
            strCategoryThumb = strCategoryThumb
        )
    }
}