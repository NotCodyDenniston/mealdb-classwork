package com.rave.mealapp.model.mapper

import com.rave.mealapp.model.local.CategoryMeal
import com.rave.mealapp.model.remote.dtos.categorymeal.CategoryMealDTO

class CategoryMealMapper : Mapper<CategoryMealDTO, CategoryMeal> {
    override fun invoke(dto: CategoryMealDTO): CategoryMeal = with(dto) {
        CategoryMeal(idMeal, strMeal, strMealThumb)
    }
}