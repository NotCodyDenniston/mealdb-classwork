package com.rave.mealapp.model.remote.dtos.categorymeal

import kotlinx.serialization.Serializable

/**
 * Represents [Meal] data coming from the network.
 */
@Serializable
data class CategoryMealDTO(
    val idMeal: String,
    val strMeal: String,
    val strMealThumb: String
)
