package com.rave.mealapp.model.remote

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.create

/**
 * Class to contain all the data associated with the retrofit instance.
 */
@OptIn(ExperimentalSerializationApi::class)
object RetrofitClass {
    private const val VERSION = "/api/json/v1/1/"
    private const val BASE_URL = "https://www.themealdb.com$VERSION"

    private val mediaType = "application/json".toMediaType()

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(Json.asConverterFactory(mediaType))
        .build()

    fun getMealService(): MealService = retrofit.create()
}
