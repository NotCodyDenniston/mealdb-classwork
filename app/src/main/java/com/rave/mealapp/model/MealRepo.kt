package com.rave.mealapp.model

import com.rave.mealapp.model.local.Category
import com.rave.mealapp.model.mapper.CategoryMapper
import com.rave.mealapp.model.mapper.CategoryMealMapper
import com.rave.mealapp.model.remote.MealService
import com.rave.mealapp.model.remote.NetworkResponse
import com.rave.mealapp.model.remote.dtos.category.CategoryResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class MealRepo(
    private val service: MealService,
    private val mapper: CategoryMapper,
    private val categoryMealMapper: CategoryMealMapper
) {

    suspend fun getMealCategories(): NetworkResponse<*> = withContext(Dispatchers.IO) {

        val mealResponse: Response<CategoryResponse> =
            service.getAllMealCategories().execute()

        return@withContext if (mealResponse.isSuccessful) {
            val categoryResponse = mealResponse.body() ?: CategoryResponse()
            val categoryList: List<Category> = categoryResponse.categories
                .map { mapper(it) }
            NetworkResponse.Success.CategorySuccess(categoryList)
        } else {
            NetworkResponse.Error(mealResponse.message())
        }
    }

    suspend fun getMealInCategory(category: Category): NetworkResponse<*> =
        withContext(Dispatchers.IO) {
            val categoryMealResponse = service.getMealFromCategory(category.strCategory).execute()
            if (categoryMealResponse.isSuccessful) {
                val mealsInCategoryResponse =
                    categoryMealResponse.body()?.categoryMeals ?: emptyList()
                NetworkResponse.Success.CategoryMealSuccess(
                    mealsInCategoryResponse.map { categoryMealMapper(it) }
                )
            } else {
                NetworkResponse.Error(categoryMealResponse.message())
            }
        }


}