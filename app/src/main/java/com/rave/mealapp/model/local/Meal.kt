package com.rave.mealapp.model.local

data class Meal(
    val dateModified: Any,
    val idMeal: String,
    val strArea: String,
    val strCategory: String,
    val strImageSource: Any,
    val strMeal: String,
    val strInstructions: String,
    val strMealThumb: String
)
