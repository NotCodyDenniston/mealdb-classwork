package com.rave.mealapp.model.remote

import com.rave.mealapp.model.remote.dtos.category.CategoryResponse
import com.rave.mealapp.model.remote.dtos.categorymeal.CategoryMealResponse
import com.rave.mealapp.model.remote.dtos.meal.MealResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MealService {

    @GET(CATEGORY_ENDPOINT)
    fun getAllMealCategories(): Call<CategoryResponse>

    @GET(MEAL_IN_CATEGORY_ENDPOINT)
    fun getMealFromCategory(@Query("c") categoryName: String):Call<CategoryMealResponse>

    @GET(MEAL_ENDPOINT)
    fun getMeal(@Query("s") mealName: String):Call<MealResponse>

    companion object {
        private const val CATEGORY_ENDPOINT = "categories.php"
        private const val MEAL_IN_CATEGORY_ENDPOINT = "filter.php"
        private const val MEAL_ENDPOINT = "search.php"
    }
}