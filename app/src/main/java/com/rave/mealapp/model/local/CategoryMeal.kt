package com.rave.mealapp.model.local

data class CategoryMeal(
    val idMeal: String,
    val strMeal: String,
    val strMealThumb: String
)
