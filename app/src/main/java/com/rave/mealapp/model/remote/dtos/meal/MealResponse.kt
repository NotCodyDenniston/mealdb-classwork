package com.rave.mealapp.model.remote.dtos.meal

/**
 * Class to represent the response object received
 * when searching for a specific meal.
 */
data class MealResponse(
    val meals: List<Meal>
)
