package com.rave.mealapp.viewmodel

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.rave.mealapp.model.MealRepo
import com.rave.mealapp.model.local.Category
import com.rave.mealapp.model.local.CategoryMeal
import com.rave.mealapp.model.remote.NetworkResponse
import com.rave.mealapp.views.Screens
import com.rave.mealapp.views.categorymealscreen.CategoryMealState
import com.rave.mealapp.views.categoryscreen.CategoryScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class MealViewModel(
    private val repo: MealRepo
) : ViewModel() {
    private val TAG = "MealViewModel"

    private val _categories: MutableStateFlow<CategoryScreenState> =
        MutableStateFlow(CategoryScreenState())
    val categoryState: StateFlow<CategoryScreenState>
        get() = _categories

    private val _categoryMealState: MutableStateFlow<CategoryMealState> =
        MutableStateFlow(CategoryMealState())
    val categoryMealState: StateFlow<CategoryMealState> get() = _categoryMealState

    private val _currentScreen: MutableState<Screens> = mutableStateOf(Screens.CategoryScreen)


    fun getCategories() = viewModelScope.launch {
        _categories.update { it.copy(isLoading = true) }
        val categories = repo.getMealCategories()
        handleResults(categories)
    }

    private fun handleResults(networkResponse: NetworkResponse<*>) {
        when (networkResponse) {
            is NetworkResponse.Error -> {
                _categories.update {
                    it.copy(
                        isLoading = false, error = networkResponse.message
                    )
                }
            }
            is NetworkResponse.Success.CategoryMealSuccess<*> -> {
                _categoryMealState.update {
                    it.copy(
                        isLoading = false,
                        categoryMeals = networkResponse.mealsInCategory as List<CategoryMeal>
                    )
                }
            }
            is NetworkResponse.Success.CategorySuccess<*> -> {
                _categories.update {
                    it.copy(
                        isLoading = false,
                        categories = networkResponse.categoryList as List<Category>
                    )
                }
            }
            else -> {
                Log.e(TAG, "getCategories: I don't feel well Mr. Stark")
            }
        }
    }


    fun getMealsInCategory(category: Category) = viewModelScope.launch {
        _categoryMealState.update { it.copy(isLoading = true) }
        val categoryMeals = repo.getMealInCategory(category)
        handleResults(categoryMeals)
    }

}

class VMlFactory(
    private val repo: MealRepo
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MealViewModel::class.java)) {
            return MealViewModel(repo) as T
        } else {
            throw IllegalArgumentException("IDK whatchu talkin' bout Willis")
        }
    }
}