package com.rave.mealapp.views.categoryscreen

import com.rave.mealapp.model.local.Category

/**
 * Class to represent the states the [CategoryScreen].
 */
data class CategoryScreenState(
    val isLoading: Boolean = false,
    val categories: List<Category> = emptyList(),
    val error: String = ""
)
