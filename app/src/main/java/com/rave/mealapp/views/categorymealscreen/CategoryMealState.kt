package com.rave.mealapp.views.categorymealscreen

import com.rave.mealapp.model.local.CategoryMeal

/**
 * State for our category meal screen.
 *
 */
data class CategoryMealState(
    val isLoading: Boolean = false,
    val categoryMeals: List<CategoryMeal> = emptyList(),
    val errMsg: String = "ErrrhMahhGAAWWD"
)
