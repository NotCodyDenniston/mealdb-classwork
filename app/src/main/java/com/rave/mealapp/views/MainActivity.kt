package com.rave.mealapp.views

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.rave.mealapp.model.MealRepo
import com.rave.mealapp.model.local.Category
import com.rave.mealapp.model.mapper.CategoryMapper
import com.rave.mealapp.model.mapper.CategoryMealMapper
import com.rave.mealapp.model.remote.RetrofitClass
import com.rave.mealapp.ui.theme.MealAppTheme
import com.rave.mealapp.viewmodel.MealViewModel
import com.rave.mealapp.viewmodel.VMlFactory
import com.rave.mealapp.views.categorymealscreen.CategoryMealScreen
import com.rave.mealapp.views.categorymealscreen.CategoryMealState
import com.rave.mealapp.views.categoryscreen.CategoryScreen
import com.rave.mealapp.views.categoryscreen.CategoryScreenState

/**
 * Starting point for our Meal app.
 */
class MainActivity : ComponentActivity() {

    private val mealViewModel by viewModels<MealViewModel> {
        val repo = MealRepo(
            RetrofitClass.getMealService(),
            CategoryMapper(),
            CategoryMealMapper()
        )
        VMlFactory(repo)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mealViewModel.getCategories()
        setContent {
            val categoryState by mealViewModel.categoryState.collectAsState()
            val categoryMealState by mealViewModel.categoryMealState.collectAsState()
            MealAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MealListApp(
                        categoryState,
                        categoryMealState,
                        onCategorySelected = { selectedCategory: Category ->
                            mealViewModel.getMealsInCategory(selectedCategory)
                        }
                    )
                }
            }
        }
    }
}

@Composable
fun MealListApp(
    categories: CategoryScreenState,
    categoryMealState: CategoryMealState,
    onCategorySelected: (category: Category) -> Unit
) {

    val navController: NavHostController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Screens.CategoryScreen.route
    ) {
        composable(Screens.CategoryScreen.route) {
            CategoryScreen(categories) { screen: Screens, category: Category ->
                onCategorySelected(category)
                navController.navigate(screen.route)
            }
        }
        composable(Screens.CategoryMealScreen.route) {
            CategoryMealScreen(categoryMealState)
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MealAppTheme {
        Greeting("Android")
    }
}
