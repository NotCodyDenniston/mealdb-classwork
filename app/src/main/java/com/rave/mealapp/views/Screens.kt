package com.rave.mealapp.views

enum class Screens(val route: String) {
    CategoryScreen("CategoryScreen"),
    CategoryMealScreen("CategoryMealScreen")
}
