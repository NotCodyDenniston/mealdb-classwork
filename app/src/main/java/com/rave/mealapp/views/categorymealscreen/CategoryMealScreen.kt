package com.rave.mealapp.views.categorymealscreen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import okhttp3.internal.notifyAll

@Composable
fun CategoryMealScreen(state: CategoryMealState) {
    LazyColumn(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        items(state.categoryMeals) { meal ->
            Text(text = "Why you no make me a ${meal.strMeal}??????")
        }
    }
}