package com.rave.mealapp.views.categoryscreen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import coil.compose.AsyncImage
import com.rave.mealapp.model.local.Category
import com.rave.mealapp.views.Screens

/**
 * Screen associated with the display and selection of [Category] objects.
 */
@Composable
fun CategoryScreen(
    state: CategoryScreenState,
    navigate: (screen: Screens, category: Category) -> Unit
) {
    LazyColumn() {
        items(state.categories) { category: Category ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround,
                modifier = Modifier.clickable {
                    navigate(Screens.CategoryMealScreen, category)
                }
            ) {
                AsyncImage(
                    model = category.strCategoryThumb,
                    contentDescription = "${category.strCategory} shown as an image."
                )
                Text(text = category.strCategory)
            }
        }
    }
}
